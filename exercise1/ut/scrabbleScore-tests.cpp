#include "gtest/gtest.h"
#include "scrabbleScore.hpp"

struct scrabbleScoreTestSuite {};

TEST(scrabbleScoreTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(scrabbleScoreTestSuite, ScoreShoulBeEqual13)
{
  ASSERT_EQ(calculate_scrabble_score("HEJ"), 13);
}

TEST(scrabbleScoreTestSuite, ScoreShoulBeEqual11)
{
  ASSERT_EQ(calculate_scrabble_score("KONRAD"), 11);
}

TEST(scrabbleScoreTestSuite, ScoreShoulBeEqual18)
{
  ASSERT_EQ(calculate_scrabble_score("MATEUSZ"), 18);
}

TEST(scrabbleScoreTestSuite, EmptyStringShouldGiveResultEqual0)
{
  ASSERT_EQ(calculate_scrabble_score(""), 0);
}


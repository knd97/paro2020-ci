#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(true);
}

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, February2ndIsThritythirdDayOfyear)
{
  ASSERT_EQ(dayOfYear(2, 2, 2020), 33);
}

TEST(DayOfYearTestSuite, March2ndIsThritythirdDayOfyear)
{
  ASSERT_EQ(dayOfYear(3, 2, 2020), 62);
}

TEST(DayOfYearTestSuite, March2ndIsThritytSecondDayOfyear)
{
  ASSERT_EQ(dayOfYear(3, 2, 2019), 61);
}
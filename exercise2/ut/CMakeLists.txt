add_executable(DayOfYearTests
  day-of-year-tests.cpp)

find_library(DayOfYear HINTS ../)
target_link_libraries (DayOfYearTests PUBLIC DayOfYear gtest_main)

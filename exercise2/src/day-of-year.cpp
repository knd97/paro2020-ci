#include "day-of-year.hpp"
#include <unordered_map>
#include <string>

namespace
{
    std::unordered_map<int, int> g_months{
        {1, 31}, {2, 28}, {3, 31}, {4, 30}, {5, 31}, {6, 30}, {7, 31}, {8, 31}, {9, 30}, {10, 31}, {11, 30}, {12, 31}};
}

int dayOfYear(int month, int dayOfMonth, int year)
{
    auto l_dayOfYear{dayOfMonth};
    if (month == 1)
    {
        return l_dayOfYear;
    }

    for (size_t i = 1; i < month; ++i)
    {
        l_dayOfYear += g_months[i];
    }

    return (month > 2 && year % 4 == 0) ? l_dayOfYear + 1 : l_dayOfYear;
}
